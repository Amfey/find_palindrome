import itertools
import unittest

# to speed up script I decide to get list of 5 digits primes
# this help me not search primes which is not time efficent
# I import list of primes from 5_digit_primes.txt
# primes list are open soure and I can download it from internet
# source: https://www.mathsisfun.com/numbers/prime-number-lists.html
# I attached file to my message

f = open("5_digit_primes.txt","r")
primes = f.read().splitlines()
f.close()

def is_palindrome(number):
    nums = str(number)
    if len(nums) % 2 == 0:
        if nums[:(len(nums)/2)] == nums[(len(nums)/2):][::-1]:
            return True
        else:
            return False
    else:
        if nums[:(len(nums) / 2)] == nums[((len(nums) / 2)+1):][::-1]:
            return True
        else:
            return False

primes = map(lambda x:int(x), primes)

primes_combinations = [a for a in itertools.combinations(primes, 2) if is_palindrome(a[0]*a[1])]

palindromes = map(lambda x: x[0]*x[1], primes_combinations)

largest_palindrome = max(palindromes)

print largest_palindrome, primes_combinations[palindromes.index(largest_palindrome)]

# answer is:
# largest_palindrome: 999949999
# multipliers are: (30109, 33211)


# Some tests for my is_palindrome function
class MyTests(unittest.TestCase):

    def test_palindrome_even_true(self):
        self.assertEqual(is_palindrome(123321), True)

    def test_palindrome_even_false(self):
        self.assertEqual(is_palindrome(123421), False)

    def test_palindrome_non_even_true(self):
        self.assertEqual(is_palindrome(1234321), True)

    def test_palindrome_non_even_false(self):
        self.assertEqual(is_palindrome(1234521), False)